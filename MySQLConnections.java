/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.*;
import javax.swing.JOptionPane;
/**
 *
 * @author Bharath
 */
public abstract class MySQLConnections {
    public static Connection getConnection(){
        Connection connection=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = (Connection)DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mov_book?autoReconnect=true&useSSL=false","root","root");
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }
    
    public static int getID(){
        int id = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = (Connection)DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mov_book?autoReconnect=true&useSSL=false","root","root");
            Statement stmt = (Statement)connection.createStatement();
            ResultSet rs = stmt.executeQuery("select SELECT TOP 1 * FROM transactions");
            while(rs.next()) {
                id = Integer.parseInt(rs.getString(1));
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return (id+1);
    }
}
